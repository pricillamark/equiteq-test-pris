</div> <!-- closes <div class=container"> -->

<!-- Footer -->
<footer class="bg-black text-white text-md-left text-left">
    <div class="container">
        <div class="row">
            <div class="footer-header col-md-8 col-12">
                <div class="footer-header-img pb-4">
                    <img src="<?php echo get_template_directory_uri() ?>/images/svg/Equitec_Logo_ReverseColour_RGB-2.svg" alt="logo">
                </div>
                <div class="footer-header-text">
                    <p>Equiteq is the leading global investment bank for the Knowledge Economy.<br>Equiteq provides merger, acquisition and advisory services.</p>
                </div>
            </div>
            <div class="footer-menu">
                <div class="row">
                    <div class="col">
                        <div class="menu-list">
                            <ul role="footer-menu-1" class="col col-md-3">
                                <li class="footer-menu-subtitle font-weight-bold" role="none"><a href="javascript:;" role="menuitem">Services</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/services/buy" role="menuitem">Buy Advisory</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/services/sell" role="menuitem">Sell Advisory</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/services/corporate-divestitures" role="menuitem">Corporate Divestitures</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/services/strategic-origination" role="menuitem">Strategic Deal Origination</a></li>
                            </ul>
                            <ul role="footer-menu-2" class="col col-md-5">
                                <li class="footer-menu-subtitle font-weight-bold" role="none"><a href="javascript:;" role="menuitem">Sectors</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/industry/technology-services-outsourcing" role="menuitem">Technology Services &amp; Outsourcing</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/industry/management-consulting" role="menuitem">Management Consulting</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/industry/engineering-consulting-services" role="menuitem">Engineering Consulting &amp; Services</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/industry/human-capital-management" role="menuitem">Human Capital Management</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/industry/saas-software" role="menuitem">Enterprise Software</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/industry/marketing-communications-information-services" role="menuitem">Marketing, Communications &amp; Information Services</a></li>
                            </ul>
                            <ul role="footer-menu-3" class="col col-md-4">
                                <li class="footer-menu-subtitle font-weight-bold" role="none"><a href="https://www.equiteq.com/deals" role="menuitem">Deals</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/resources" role="menuitem">Resources</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/team" role="menuitem">Our Team</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/ma-advisory" role="menuitem">About</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/careers-at-equiteq" role="menuitem">Careers</a></li>
                                <li class="hs-menu-item hs-menu-depth-1" role="none"><a href="https://www.equiteq.com/contact-us" role="menuitem">Contact Us</a></li>
                            </ul>
                            </ul>
                        </div>
                        <div class="footer-socials">
                            <ul>
                                <li><a class="youtube" href="https://www.youtube.com/user/equiteq" target="_blank" style="background: url('<?php echo get_template_directory_uri() ?>/images/icon_youtube.png') no-repeat;"></a></li>
                                <li><a class="twitter" href="https://twitter.com/EquiteqGlobal" target="_blank" style="background: url('<?php echo get_template_directory_uri() ?>/images/icon_twitter.png') no-repeat;"></a></li>
                                <li><a class="slideshare" href="https://www.slideshare.net/EquiteqEdge" target="_blank" style="background: url('<?php echo get_template_directory_uri() ?>/images/icon_slideshare.png') no-repeat;"></a></li>
                                <li><a class="linkedin" href="https://www.linkedin.com/company/equiteq-llp" target="_blank" style="background: url('<?php echo get_template_directory_uri() ?>/images/icon_linkedin.png') no-repeat;"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<?php
// $page2 = get_post($id);
// hm_get_template_part('template-parts/cta/modal-cta-expert', ['page' => $page2]);
?>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script> -->

<?php wp_footer()?>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

</body>

</html>